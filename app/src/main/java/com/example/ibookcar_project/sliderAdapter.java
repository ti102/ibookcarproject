package com.example.ibookcar_project;
import android.content.Context;
import android.view.LayoutInflater;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import  androidx.viewpager.widget.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.viewpager.widget.PagerAdapter;

public class sliderAdapter extends PagerAdapter {
    Context context;
    LayoutInflater layoutInflater;
    public sliderAdapter(Context context){

        this.context = context;

    };
    //   Arrays
    public int[] slide_images = {
            R.drawable.logo_ibookcar,
            R.drawable.begin_img1,
            R.drawable.begin_img2,
            R.drawable.begin_img3,
    };
    public String[] slide_contents ={
            "Amet minim mollit non deserunt ullamco est sit aliqua dolor do axamet sint. Velit officia do amet sint. Velit officia",
            "Amet minim mollit non deserunt ullamco est sit aliqua dolor do axamet sint. Velit officia do amet sint. Velit officia",
            "Amet minim mollit non deserunt ullamco est sit aliqua dolor do axamet sint. Velit officia do amet sint. Velit officia",
            "Amet minim mollit non deserunt ullamco est sit aliqua dolor do axamet sint. Velit officia do amet sint. Velit officia"
    };
    @Override
    public int getCount(){
        return slide_contents.length;
    }
    @Override
    public boolean isViewFromObject(View view,Object object) {
        return view ==(RelativeLayout) object;

    }

    public Object instantiateItem(ViewGroup container, int position){
        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.slide_layout, container, false);
        ImageView sliderImageView = (ImageView) view.findViewById(R.id.slide_image);
        TextView slide_content = (TextView) view.findViewById(R.id.slide_content);

        sliderImageView.setImageResource(slide_images[position]);
        slide_content.setText(slide_contents[position]);

        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object){
        container.removeView((RelativeLayout)object);
    }
}
